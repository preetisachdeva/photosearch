app.controller("searchPhotoController",['$rootScope','$scope','$http','$modal', '$log', function($rootScope, $scope, $http,$modal, $log) {
   $scope.pagePerPage =  30; // show 10 pics per page
   var resultsPhoto = [];
   $scope.totalSizeImg = 0;  
   $scope.allResImg = 1;    // Using for check condition on loading images
   $scope.page = 1;         // On first load  the images will show for page 1
   $scope.filterPhotos = [];
   $scope.checkInKeyword = 0;
   $scope.showImage = false;
   // function for filter the images by name
   $scope.searchPhotos = function() {
        var limit_start = $scope.filterPhotos.length;
        $scope.showImage = true;
        // Blank the array and variables when user type different names
        if(($scope.checkInKeyword == 1) && ($scope.keywordChange != $scope.keyword)){
             $scope.page = 1;
             $scope.totalSizeImg = 0;
             $scope.allResImg = 1;
             $scope.filterPhotos = [];
        }
        // check for cookie with $scope.keyword and loading more images on scrolling
        if($scope.keyword !== '' && $scope.keyword != undefined) {
            if ((( $scope.totalSizeImg > limit_start) || $scope.totalSizeImg == 0 ) && $scope.allResImg == 1) {
                 $scope.allResImg = 0;
                 $http.get("https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=ef5c3d5c609e167ca47524d317140918&tags="+$scope.keyword+"&per_page="+$scope.pagePerPage+"&format=json&nojsoncallback=1&page="+$scope.page).then(function (response) {
                    $scope.totalSizeImg = response.data.photos.total;
                    $scope.showImage = false;
                    $scope.allResImg = 1;
                    $scope.checkInKeyword = 1;
                    $scope.keywordChange = $scope.keyword;
                    $scope.filterPhotos = $scope.filterPhotos.concat(response.data.photos.photo); 
                    $scope.page++;
                    // set the localStorage for entered type name
                    localStorage.setItem("lastSearch", $scope.keywordChange);
                },function(error){
                    $scope.filterPhotos =[];
                });
            };
      };
   };
   // Call on scrolling down
   $scope.loadMore = function(){
       if($scope.keyword !== '' || $scope.keyword !== undefined){
          $scope.searchPhotos();
       }
   }
   // Check for localStorage value for use type name
   if(localStorage.getItem("lastSearch") !== '' || localStorage.getItem("lastSearch") !== null || localStorage.getItem("lastSearch") !== undefined)
   {    
        $scope.keyword = localStorage.getItem("lastSearch");
        // call for show the images
        $scope.searchPhotos(); 
   }
   // Open modal/pop-up on click on image
   $scope.open = function (index) {
        $scope.getIndex = index;
        var modalInstance= $modal.open({
            // loads the template
            backdrop: true, // setting backdrop allows us to close the modal window on clicking outside the modal window
            scope: $scope,
            link: function(scope) {
                scope.cancel = function() {
                    scope.$dismiss('cancel');
                };
            },
            template:  "<div>" + 
                "<div class='modal-header'>" +
                "<h3 ng-bind='dialogTitle'></h3>" +  "<img src='http://farm{{filterPhotos[getIndex].farm}}.staticflickr.com/{{filterPhotos[getIndex].server}}/{{filterPhotos[getIndex].id}}_{{filterPhotos[getIndex].secret}}.jpg'/>" +
                "</div>" +
                "<div class='text-align-center' ng-bind='filterPhotos[getIndex].title'></div>"+
            "</div>",
        }); //end of modal.open
        modalInstance.result.then(function (selectedItem) {
         }, function () {
             $log.info('Modal dismissed at: ' + new Date());
        }); // call on close modal outside image
    }; // end of open function
}]);